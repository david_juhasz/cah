package com.valami.cah.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.valami.cah.repository.PlayerRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ListPlayersService {
	
	private final PlayerRepository playerRepository;
	
	public List<String> listPlayers(Long gameId) {
		return playerRepository.findByGameId(gameId).stream()
				.map(player -> player.getName())
				.collect(Collectors.toList());
	}

}
