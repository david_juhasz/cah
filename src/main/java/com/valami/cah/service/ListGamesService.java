package com.valami.cah.service;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.valami.cah.model.ListGamesResponseElement;
import com.valami.cah.repository.GameEntity;
import com.valami.cah.repository.GameRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ListGamesService {

	private final GameRepository gameRepository;
	
	public List<ListGamesResponseElement> listJoinableGames() {
		List<GameEntity> joinableGames = gameRepository.findByStarted(false);
		return transform(joinableGames);
	}
	
	private List<ListGamesResponseElement> transform(List<GameEntity> gameEntities) {
		List<ListGamesResponseElement> games = new ArrayList<>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		for (GameEntity gameEntity : gameEntities) {
			games.add(ListGamesResponseElement.builder()
					.id(gameEntity.getId())
					.creatorName(gameEntity.getCreatorName())
					.created(gameEntity.getCreated().format(formatter))
					.build());
		}
		return games;
	}
}
