package com.valami.cah.service;

import org.springframework.stereotype.Service;

import com.valami.cah.repository.PlayerRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PlayerStatusService {

	private final PlayerRepository playerRepository;
	
	public Boolean isPlayerQuestioner(Long playerId) {
		return playerRepository.findById(playerId).get().getQuestioner();
	}
}
