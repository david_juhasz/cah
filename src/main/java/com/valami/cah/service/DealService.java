package com.valami.cah.service;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.valami.cah.repository.AnswerEntity;
import com.valami.cah.repository.AnswerRepository;
import com.valami.cah.repository.PlayerEntity;
import com.valami.cah.repository.PlayerRepository;
import com.valami.cah.repository.QuestionEntity;
import com.valami.cah.repository.QuestionRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DealService {
	private static final int CARDS_BY_PLAYER = 10;
	
	private final PlayerRepository playerRepository;
	private final QuestionRepository questionRepository;
	private final AnswerRepository answerRepository;
	
	public void dealQuestion(Long gameId) {
	    Random rand = new Random();
		List<QuestionEntity> questions = questionRepository.findByGameId(gameId);
		QuestionEntity currentQuestion = questions.get(rand.nextInt(questions.size()));
		currentQuestion.setCurrent(true);
		questionRepository.save(currentQuestion);
	}

	public void dealAnswers(Long gameId) {
		List<AnswerEntity> cards = answerRepository.findByGameId(gameId);
		Collections.shuffle(cards);
		List<PlayerEntity> players = playerRepository.findByGameId(gameId);
		for (PlayerEntity player : players) {
			dealToPlayer(player, cards);
		}
	}
	
	private void dealToPlayer(PlayerEntity player, List<AnswerEntity> cards) {
		List<AnswerEntity> playersCards = cards.stream()
				.filter(card -> player.equals(card.getPlayer()))
				.collect(Collectors.toList());
		List<AnswerEntity> unownedCards = cards.stream()
				.filter(card -> card.getPlayer() == null)
				.collect(Collectors.toList());
		while (playersCards.size() < CARDS_BY_PLAYER) {
			AnswerEntity chosenCard = unownedCards.get(0);
			chosenCard.setPlayer(player);
			answerRepository.save(chosenCard);
			playersCards.add(chosenCard);
			unownedCards.remove(chosenCard);
		}
	}
}
