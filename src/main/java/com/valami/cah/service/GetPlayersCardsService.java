package com.valami.cah.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.valami.cah.model.PlayerCardsResponseElement;
import com.valami.cah.repository.AnswerEntity;
import com.valami.cah.repository.AnswerRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GetPlayersCardsService {

	private final AnswerRepository answerRepository;
	
	public List<PlayerCardsResponseElement> getPlayersCards(Long gameId, Long playerId) {
		List<AnswerEntity> answers = answerRepository.findByGameIdAndPlayerId(gameId, playerId);
		List<PlayerCardsResponseElement> playerCards = new ArrayList<>();
		for (AnswerEntity answer : answers) {
			playerCards.add(map(answer));
		}
		return playerCards;
	}
	
	private PlayerCardsResponseElement map(AnswerEntity answer) {
		return PlayerCardsResponseElement.builder()
				.id(answer.getId())
				.text(answer.getText())
				.build();
	}
}
