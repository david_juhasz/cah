package com.valami.cah.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.valami.cah.model.CloseRoundRequest;
import com.valami.cah.repository.PlayerEntity;
import com.valami.cah.repository.PlayerRepository;
import com.valami.cah.repository.QuestionRepository;
import com.valami.cah.repository.RoundRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CloseRoundService {

	private final PlayerRepository playerRepository;
	private final RoundRepository roundRepository;
	private final QuestionRepository questionRepository;
	private final DealService dealService;

	@Transactional
	public void closeRound(CloseRoundRequest request) {
		Long gameId = request.getGameId();
		passQuestionerRight(gameId, request.getWinningPlayerId());
		clearRoundData(gameId);
		deleteCurrentQuestion(gameId);
		redeal(gameId);
	}
	
	private void passQuestionerRight(Long gameId, Long winningPlayerId) {
	 	PlayerEntity previousQuestioner = playerRepository.findByGameIdAndQuestioner(gameId, true);
	 	previousQuestioner.setQuestioner(false);
	 	playerRepository.save(previousQuestioner);
	 	PlayerEntity nextQuestioner = playerRepository.findById(winningPlayerId).get();
	 	nextQuestioner.setQuestioner(true);
	 	playerRepository.save(nextQuestioner);
	}
	
	private void clearRoundData(Long gameId) {
		roundRepository.deleteByGameId(gameId);
	}

	private void deleteCurrentQuestion(Long gameId) {
		questionRepository.deleteByGameIdAndCurrent(gameId, true);
	}
	
	private void redeal(Long gameId) {
		dealService.dealQuestion(gameId);
		dealService.dealAnswers(gameId);
	}
}
