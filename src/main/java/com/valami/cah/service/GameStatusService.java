package com.valami.cah.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.valami.cah.exception.CahException;
import com.valami.cah.repository.GameEntity;
import com.valami.cah.repository.GameRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GameStatusService {

	private final GameRepository gameRepository;
	
	public boolean isGameStarted(Long gameId) {
		Optional<GameEntity> game = gameRepository.findById(gameId);
		if (game.isEmpty()) {
			throw new CahException("Game not found");
		}
		return gameRepository.findById(gameId).get().getStarted();
	}
}
