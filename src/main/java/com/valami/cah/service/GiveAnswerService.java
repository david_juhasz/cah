package com.valami.cah.service;

import org.springframework.stereotype.Service;

import com.valami.cah.model.GiveAnswerRequest;
import com.valami.cah.repository.AnswerEntity;
import com.valami.cah.repository.AnswerRepository;
import com.valami.cah.repository.GameEntity;
import com.valami.cah.repository.GameRepository;
import com.valami.cah.repository.PlayerEntity;
import com.valami.cah.repository.PlayerRepository;
import com.valami.cah.repository.RoundEntity;
import com.valami.cah.repository.RoundRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GiveAnswerService {

	private final GameRepository gameRepository;
	private final AnswerRepository answerRepository;
	private final PlayerRepository playerRepository;
	private final RoundRepository roundRepository;
	
	public void giveAnswer(GiveAnswerRequest request) {
		PlayerEntity player = playerRepository.findById(request.getPlayerId()).get();
		GameEntity game = gameRepository.findById(request.getGameId()).get();
		for(Long answerId : request.getAnswerIds()) {
			AnswerEntity answer = answerRepository.findById(answerId).get();
			RoundEntity round = RoundEntity.builder()
					.game(game)
					.player(player)
					.answer(answer)
					.answerIndex(request.getAnswerIds().indexOf(answerId))
					.build();
			roundRepository.save(round);
			answer.setPlayer(null);
			answerRepository.save(answer);
		}
	}
	
}
