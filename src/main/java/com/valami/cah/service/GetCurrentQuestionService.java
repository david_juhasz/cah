package com.valami.cah.service;

import org.springframework.stereotype.Service;

import com.valami.cah.repository.QuestionEntity;
import com.valami.cah.repository.QuestionRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GetCurrentQuestionService {
	
	private final QuestionRepository questionRepository;
	
	public String getCurrentQuestion(Long gameId) {
		QuestionEntity currentQuestion = questionRepository.findByGameIdAndCurrent(gameId, true);
		return currentQuestion.getText();
	}

}
