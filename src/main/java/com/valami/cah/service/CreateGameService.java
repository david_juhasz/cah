package com.valami.cah.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;

import org.springframework.stereotype.Service;

import com.valami.cah.model.CreateGameRequest;
import com.valami.cah.model.CreateGameResponse;
import com.valami.cah.repository.AnswerEntity;
import com.valami.cah.repository.AnswerRepository;
import com.valami.cah.repository.GameEntity;
import com.valami.cah.repository.GameRepository;
import com.valami.cah.repository.PlayerEntity;
import com.valami.cah.repository.PlayerRepository;
import com.valami.cah.repository.QuestionEntity;
import com.valami.cah.repository.QuestionRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CreateGameService {
	
	private final GameRepository gameRepository;
	private final PlayerRepository playerRepository;
	private final QuestionRepository questionRepository;
	private final AnswerRepository answerRepository;

	public CreateGameResponse createGame(CreateGameRequest request) {
		GameEntity game = GameEntity.builder()
				.created(LocalDateTime.now())
				.creatorName(request.getCreatorName())
				.started(false)
				.build();
		gameRepository.save(game);
		PlayerEntity player = PlayerEntity.builder()
				.name(request.getCreatorName())
				.game(game)
				.creator(true)
				.questioner(true)
				.build();
		playerRepository.save(player);
		loadQuestions(game);
		loadAnswers(game);
		return CreateGameResponse.builder()
				.gameId(game.getId())
				.playerId(player.getId())
				.build();
	}
	
	private void loadQuestions(GameEntity game) {
		BufferedReader reader;
		try {
			File file = new File(getClass().getClassLoader().getResource("questions.txt").getFile());
			reader = new BufferedReader(new FileReader(file));
			String line = reader.readLine();
			while (line != null) {
				QuestionEntity question = QuestionEntity.builder()
						.text(line)
						.game(game)
						.current(false)
						.build();
				questionRepository.save(question);
				line = reader.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void loadAnswers(GameEntity game) {
		BufferedReader reader;
		try {
			File file = new File(getClass().getClassLoader().getResource("answers.txt").getFile());
			reader = new BufferedReader(new FileReader(file));
			String line = reader.readLine();
			while (line != null) {
				AnswerEntity answer = AnswerEntity.builder()
						.text(line)
						.game(game)
						.build();
				answerRepository.save(answer);
				line = reader.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
