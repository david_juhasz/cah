package com.valami.cah.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.valami.cah.model.GetAnswersForRoundResponseElement;
import com.valami.cah.repository.RoundEntity;
import com.valami.cah.repository.RoundRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GetAnswersForRoundService {
	
	private final RoundRepository roundRepository;

	public List<GetAnswersForRoundResponseElement> getAnswersForRound(Long gameId) {
		List<GetAnswersForRoundResponseElement> answers = new ArrayList<>();
		List<RoundEntity> roundEntities = roundRepository.findByGameIdOrderByAnswerIndex(gameId);
		Set<Long> playerIds = new HashSet<>(); 
		roundEntities.forEach(round -> playerIds.add(round.getPlayer().getId()));
		for (Long playerId : playerIds) {
			answers.add(getAnswersByPlayer(roundEntities, playerId));
		}
		return answers;
	}
	
	private GetAnswersForRoundResponseElement getAnswersByPlayer(List<RoundEntity> roundEntities, Long playerId) {
		List<RoundEntity> answersByPlayer = roundEntities.stream()
				.filter(round -> round.getPlayer().getId().equals(playerId))
				.collect(Collectors.toList());
		return GetAnswersForRoundResponseElement.builder()
				.playerId(playerId)
				.playerName(answersByPlayer.get(0).getPlayer().getName())
				.answers(answersByPlayer.stream()
						.map(answer -> answer.getAnswer().getText())
						.collect(Collectors.toList()))
				.build();
	}
}
