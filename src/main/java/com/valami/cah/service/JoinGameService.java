package com.valami.cah.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.valami.cah.exception.CahException;
import com.valami.cah.model.JoinGameRequest;
import com.valami.cah.repository.GameEntity;
import com.valami.cah.repository.GameRepository;
import com.valami.cah.repository.PlayerEntity;
import com.valami.cah.repository.PlayerRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class JoinGameService {

	private final GameRepository gameRepository;
	private final PlayerRepository playerRepository;
	
	public Long join(JoinGameRequest request) {
		Optional<GameEntity> game = gameRepository.findById(request.getGameId());
		if (!game.isPresent()) {
			throw new CahException("There is no game with the given ID you can join.");
		}
		if (game.get().getStarted()) {
			throw new CahException("Game is already started");
		}
		PlayerEntity player = PlayerEntity.builder()
				.name(request.getPlayerName())
				.game(game.get())
				.creator(false)
				.questioner(false)
				.build();
		playerRepository.save(player);
		return player.getId();
	}
}
