package com.valami.cah.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.valami.cah.exception.CahException;
import com.valami.cah.model.StartGameRequest;
import com.valami.cah.repository.GameEntity;
import com.valami.cah.repository.GameRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class StartGameService {

	private final GameRepository gameRepository;
	private final DealService dealService;
	
	public void start(StartGameRequest request) {
		Optional<GameEntity> gameOptional = gameRepository.findById(request.getGameId());
		if (gameOptional.isEmpty()) {
			throw new CahException("Game not found");
		}
		GameEntity game = gameOptional.get();
		if (game.getStarted()) {
			throw new CahException("Game is already started");
		}
		game.setStarted(true);
		gameRepository.save(game);
		dealService.dealQuestion(request.getGameId());
		dealService.dealAnswers(request.getGameId());
	}
}
