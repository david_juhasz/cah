package com.valami.cah;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CahApplication {

	public static void main(String[] args) {
		SpringApplication.run(CahApplication.class, args);
	}

}
