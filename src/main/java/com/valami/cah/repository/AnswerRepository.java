package com.valami.cah.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerRepository extends CrudRepository<AnswerEntity, Long> {

	List<AnswerEntity> findByPlayer(PlayerEntity player);
	
	List<AnswerEntity> findByGameId(Long gameId);
	
	List<AnswerEntity> findByGameIdAndPlayerId(Long gameId, Long playerId);
}
