package com.valami.cah.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends CrudRepository<PlayerEntity, Long> {

	List<PlayerEntity> findByGameId(Long gameId);
	
	PlayerEntity findByGameIdAndQuestioner(Long gameId, Boolean questioner);
}
