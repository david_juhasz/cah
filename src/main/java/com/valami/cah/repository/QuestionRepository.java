package com.valami.cah.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends CrudRepository<QuestionEntity, Long> {

	List<QuestionEntity> findByGameId(Long gameId);
	
	QuestionEntity findByGameIdAndCurrent(Long gameId, Boolean current);
	
	void deleteByGameIdAndCurrent(Long gameId, Boolean current);
}
