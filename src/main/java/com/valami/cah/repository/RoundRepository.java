package com.valami.cah.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoundRepository extends CrudRepository<RoundEntity, Long> {
	
	List<RoundEntity> findByGameIdOrderByAnswerIndex(Long gameId);
	
	void deleteByGameId(Long gameId);
}
