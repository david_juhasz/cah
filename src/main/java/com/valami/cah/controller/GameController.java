package com.valami.cah.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.valami.cah.model.CloseRoundRequest;
import com.valami.cah.model.CreateGameRequest;
import com.valami.cah.model.CreateGameResponse;
import com.valami.cah.model.GetAnswersForRoundResponseElement;
import com.valami.cah.model.GiveAnswerRequest;
import com.valami.cah.model.JoinGameRequest;
import com.valami.cah.model.ListGamesResponseElement;
import com.valami.cah.model.PlayerCardsResponseElement;
import com.valami.cah.model.StartGameRequest;
import com.valami.cah.service.CloseRoundService;
import com.valami.cah.service.CreateGameService;
import com.valami.cah.service.GameStatusService;
import com.valami.cah.service.GetAnswersForRoundService;
import com.valami.cah.service.GetCurrentQuestionService;
import com.valami.cah.service.GetPlayersCardsService;
import com.valami.cah.service.GiveAnswerService;
import com.valami.cah.service.JoinGameService;
import com.valami.cah.service.ListGamesService;
import com.valami.cah.service.ListPlayersService;
import com.valami.cah.service.PlayerStatusService;
import com.valami.cah.service.StartGameService;

import lombok.RequiredArgsConstructor;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class GameController {
	private final CreateGameService createGameService;
	private final JoinGameService joinGameService;
	private final StartGameService startGameService;
	private final ListGamesService listGamesService;
	private final ListPlayersService listPlayersService;
	private final GameStatusService gameStatusService;
	private final GetPlayersCardsService getPlayersCardsService;
	private final GetCurrentQuestionService getCurrentQuestionService;
	private final GiveAnswerService giveAnswerService;
	private final GetAnswersForRoundService getAnswersForRoundService;
	private final CloseRoundService closeRoundService;
	private final PlayerStatusService playerStatusService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public CreateGameResponse create(@RequestBody CreateGameRequest request) {
		return createGameService.createGame(request);
	}

	@RequestMapping(value = "/join", method = RequestMethod.POST)
	public Long join(@RequestBody JoinGameRequest request) {
		return joinGameService.join(request);
	}

	@RequestMapping(value = "/start", method = RequestMethod.POST)
	public void start(@RequestBody StartGameRequest request) {
		startGameService.start(request);
	}

	@RequestMapping(value = "/listJoinableGames", method = RequestMethod.GET)
	public List<ListGamesResponseElement> listJoinableGames() {
		return listGamesService.listJoinableGames();
	}

	@RequestMapping(value = "/listPlayers", method = RequestMethod.GET)
	public List<String> listPlayers(Long gameId) {
		return listPlayersService.listPlayers(gameId);
	}

	@RequestMapping(value = "/isGameStarted", method = RequestMethod.GET)
	public Boolean isGameStarted(Long gameId) {
		return gameStatusService.isGameStarted(gameId);
	}

	@RequestMapping(value = "/getPlayersCards", method = RequestMethod.GET)
	public List<PlayerCardsResponseElement> getPlayersCards(Long gameId, Long playerId) {
		return getPlayersCardsService.getPlayersCards(gameId, playerId);
	}

	@RequestMapping(value = "/getCurrentQuestion", method = RequestMethod.GET)
	public String getCurrentQuestion(Long gameId) {
		return getCurrentQuestionService.getCurrentQuestion(gameId);
	}

	@RequestMapping(value = "/giveAnswer", method = RequestMethod.POST)
	public void giveAnswer(@RequestBody GiveAnswerRequest request) {
		giveAnswerService.giveAnswer(request);
	}

	@RequestMapping(value = "/getAnswersForRound", method = RequestMethod.GET)
	public List<GetAnswersForRoundResponseElement> getAnswersForRound(Long gameId) {
		return getAnswersForRoundService.getAnswersForRound(gameId);
	}

	@RequestMapping(value = "/closeRound", method = RequestMethod.POST)
	public void closeRound(@RequestBody CloseRoundRequest request) {
		closeRoundService.closeRound(request);
	}

	@RequestMapping(value = "/isPlayerQuestioner", method = RequestMethod.GET)
	public Boolean isPlayerQuestioner(Long playerId) {
		return playerStatusService.isPlayerQuestioner(playerId);
	}
}
