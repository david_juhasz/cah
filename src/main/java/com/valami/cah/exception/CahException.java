package com.valami.cah.exception;

public class CahException extends RuntimeException {

	public CahException(String message) {
		super(message);
	}
}
