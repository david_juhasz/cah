package com.valami.cah.model;

import lombok.Data;

@Data
public class CloseRoundRequest {

	private Long gameId;
	private Long winningPlayerId;
}
