package com.valami.cah.model;

import lombok.Data;

@Data
public class StartGameRequest {

	private Long gameId;
}
