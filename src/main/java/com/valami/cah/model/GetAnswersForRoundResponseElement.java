package com.valami.cah.model;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetAnswersForRoundResponseElement {

	private Long playerId;
	private String playerName;
	private List<String> answers;
}
