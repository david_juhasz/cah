package com.valami.cah.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateGameResponse {

	private Long gameId;
	private Long playerId;
}
