package com.valami.cah.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ListGamesResponseElement {

	private Long id;
	private String creatorName;
	private String created;
}
