package com.valami.cah.model;

import java.util.List;

import lombok.Data;

@Data
public class GiveAnswerRequest {

	private Long gameId;
	private Long playerId;
	private List<Long> answerIds;
}
