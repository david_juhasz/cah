package com.valami.cah.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PlayerCardsResponseElement {

	private Long id;
	private String text;
}
