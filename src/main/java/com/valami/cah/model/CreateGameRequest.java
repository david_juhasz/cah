package com.valami.cah.model;

import lombok.Data;

@Data
public class CreateGameRequest {

	private String creatorName;
}
