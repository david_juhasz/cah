package com.valami.cah.model;

import lombok.Data;

@Data
public class JoinGameRequest {

	private String playerName;
	private Long gameId;
}
